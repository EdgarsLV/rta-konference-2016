# RTA conference 2016
## « One-dimensional boundary field problem analytic solution realization using MATLAB and .NET C# environment »

### Screenshots
![Main Window](https://bitbucket.org/neonuni/rta-konference-2016/raw/master/Screenshots/1.png)

![Results Window](https://bitbucket.org/neonuni/rta-konference-2016/raw/master/Screenshots/2_1.png)
![Matrix Window](https://bitbucket.org/neonuni/rta-konference-2016/raw/master/Screenshots/2_2.png)

### Requirements & Dependencies
The code was written in Visual Studio 2015, using **.NET 4.5** and C#6 features.  
To compile the app, open the solution(.sln) and press Build or Start (whichever you prefer)  
The application requires matlab to actually function, it was developed and tested with **Matlab r2015a**, other versions might or might not work.

*Contact:*
[neonschoolstash@gmail.com](mailto:neonschoolstash@gmail.com)

## References
* About MATLAB http://se.mathworks.com/products/matlab/
* Kalis, H., Kangro, I. Datorprogrammas MATLAB lietošana matemātikas mācības procesā, Rēzekne, RA Izdevniecība, 2010.
* C# Language https://msdn.microsoft.com/en-us/en-en/library/kx37x362.aspx
* Overview of the .NET Framework https://msdn.microsoft.com/en-us/library/zw4w595w(v=vs.110).aspx
* Kalis, H., Kangro, I. (2015) ANALYTICAL SOLUTION FOR 3D MODEL OF PEAT BLOCKS. Proc. of the Int. conf.: 14th International Scientific Conference ENGINEERING FOR RURAL DEVELOPMENT, May 20-22, 2015, Jelgava, Latvia, Volume 14
* Teirumnieka, Ē., Kangro, I., Teirumnieks, E., Kalis, H. (2015) The analytical solution of the 3D model with Robin's boundary conditions for 2 peat layers. Proc. of the 10-th int. Scientific and Practical Conference "Environment. Technology. Resources." Volume III, Rezekne Higher Education institution. http://journals.ru.lv/index.php/ETR/article/viewFile/618/714
* Kalis, H., Kangro, I. Matemātiskās metodes inženierzinātnēs, mācību līdzeklis; Rēzekne: RA Izdevniecība, 2004.
* What is COM? https://www.microsoft.com/com/default.mspx
* MATLAB COM automation server http://se.mathworks.com/help/matlab/call-matlab-com-automation-server.html
* Call MATLAB funcion from C# client http://se.mathworks.com/help/matlab/matlab_external/call-matlab-function-from-c-client.html