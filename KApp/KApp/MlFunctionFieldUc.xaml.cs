﻿// https://bitbucket.org/neonuni/konf2016/
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.IO;

namespace KApp
{
    public partial class MlFunctionFieldUc : UserControl
    {
        private readonly Dictionary<string, TextBox> _argFields = new Dictionary<string, TextBox>();
        private readonly MlFunctionInfo _mlFuncInfo;
        public string FuncName { get; }

        private readonly bool _funcMode = true;
        private readonly string _txtpath;

        public MlFunctionFieldUc(MlFunctionInfo f)
        {
            _mlFuncInfo = f;
            FuncName = _mlFuncInfo.Name.Replace("_", "__");
            InitializeComponent();
            DataContext = this;
            AddArgs();
        }

        public MlFunctionFieldUc(string textFilePath)
        {
            _funcMode = false;
            _txtpath = textFilePath;
            var fileName = Path.GetFileName(textFilePath);
            if (fileName != null) FuncName = fileName.Replace("_", "__");
            InitializeComponent();
            DataContext = this;
            BtnRun.Content = "Read";
        }

        private void AddArg(string name, string defaultval = "")
        {
            var label = new Label() {Content = name + "=", VerticalAlignment = VerticalAlignment.Center};
            var tbox = new TextBox() {Text = defaultval, Width = 50, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Left};
            _argFields.Add(name, tbox);
            DockPanelArgs.Children.Add(label);
            DockPanelArgs.Children.Add(tbox);
        }

        private void AddArgs()
        {
            foreach (var v in _mlFuncInfo.OverloadArgs)
            {
                AddArg(v.Key, v.Value);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_funcMode)
            {
                var c = _mlFuncInfo.MakeCallableString(_argFields.Select(v => v.Value.Text).ToArray());
                var r = AppCore.Exec(c);
                var w = new ResultsWindow($"{c} Result",r);
                w.Show();
            }
            else
            {
                var t = File.ReadAllText(_txtpath);
                var w = new ResultsWindow($"{FuncName} Result", t);
                w.Show();
            }
        }
    }
}
