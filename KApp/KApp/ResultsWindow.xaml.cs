﻿// https://bitbucket.org/neonuni/konf2016/
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace KApp
{
    public partial class ResultsWindow : Window
    {

        public string RawDataText { get; private set; }

        public List<MlDataContainer> MlData { get; private set; } = new List<MlDataContainer>();

        public ResultsWindow(string title, string rawdata)
        {
            RawDataText = rawdata;
            InitializeComponent();
            Title = title;
            DataContext = this;
            MlData = MlDataContainer.ReadRawData(rawdata);
            DataGridData.ItemsSource = MlData;
        }

        private void BtnRawOutput_Click(object sender, RoutedEventArgs e)
        {
            DataGridData.Visibility = DataGridData.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            TextBoxData.Visibility = TextBoxData.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            BtnRawOutput.Content = TextBoxData.Visibility == Visibility.Visible ? "Grid View" : "Raw Output";
        }

        private void BtnDuplicate_Click(object sender, RoutedEventArgs e)
        {
            var w = new ResultsWindow(Title, RawDataText);
            w.Show();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var s = (ScrollViewer)sender;
            s.ScrollToVerticalOffset(s.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void BtnSaveToDisk_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog { Filter = "Text file (*.txt)|*.txt" };
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, RawDataText);
        }

        protected void OnMouseDoubleClick_DataGrid(object sender, MouseButtonEventArgs args)
        {
            //if (DataGridData.SelectedItem == null) return;
            var s = DataGridData.SelectedItem as MlDataContainer;
            if (!(s is MlDataMatrix)) return;
            var w = new ResultsMatrixWindow(s as MlDataMatrix);
            w.Show();
        }
    }
}
