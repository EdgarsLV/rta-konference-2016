﻿// https://bitbucket.org/neonuni/konf2016/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace KApp
{
    public abstract class MlDataContainer
    {
        public string Name { get; protected set; }
        public virtual string Value { get; protected set; }
        public bool Success;

        public static List<MlDataContainer> ReadRawData(string data)
        {
            var list = new List<MlDataContainer>();

            var blocks = new List<string>();
            data = data.Replace("\r", "");
            var lines = data.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries).ToList();
            var sb = new StringBuilder();
            bool metFirst = false;

            foreach (var l in lines)
            {
                if (l.Contains("="))
                {
                    if (!metFirst)
                    {
                        metFirst = true;
                        sb.Append(l.Replace("=","").Trim());
                        sb.Append("\n");
                        continue;
                    }
                    sb.Remove(sb.Length - 1, 1); // remove last endline
                    blocks.Add(sb.ToString());
                    sb.Clear();
                    sb.Append(l.Replace("=", "").Trim());
                    sb.Append("\n");
                    continue;
                }
                if (string.IsNullOrWhiteSpace(l.Trim()))
                    continue;
                sb.Append(l.Trim());
                sb.Append("\n");
            }
            sb.Remove(sb.Length - 1, 1); // remove last endline
            blocks.Add(sb.ToString());


            foreach (var block in blocks)
            {
                var blines = block.Split('\n');
                if (blines.Length <= 1) // block only has name or nothing, can't do much with that...
                    continue;
                if (blines.Length == 2) // 2 lines, 1 - name, 2 - either single digit or horizontal array
                {
                    if (blines[1].Trim().Contains(" ")) // contains space, probably a horizontal array
                    {
                        list.Add(new MlDataNumArray(block));
                        continue;
                    }
                    list.Add(new MlDataNum(block)); // no spaces, one line, probably a number
                    continue;
                }
                // more than 2 lines, either vertical array or a matrix
                if (blines[1].Trim().Contains(" ") && !blines[1].Trim().Contains("*")) // 2+ lines, also has spaces, matrix
                {
                    list.Add(new MlDataMatrix(block));
                    continue;
                }
                list.Add(new MlDataNumArray(block)); // 2+ lines but no spaces, vertical array
            }

            //foreach (var bl in blocks)
            //{
            //    Trace.WriteLine(bl.Count(x=>x=='\n') + " " + bl);
            //}
            return list;
        }
    }


    public class MlDataNum : MlDataContainer
    {
        public double Number;

        public override string Value
        {
            get { return Number.ToString(CultureInfo.InvariantCulture); }

            protected set
            {
                base.Value = value;
            }
        }

        public MlDataNum(string rawblock)
        {
            var l = rawblock.Split('\n');
            Name = l[0];
            double n;
            if (double.TryParse(l[1], NumberStyles.Any, CultureInfo.InvariantCulture, out n))
            {
                Number = n;
                Success = true;
            }
            else
            {
                Number = -1;
            }
        }
    }

    public class MlDataNumArray : MlDataContainer
    {
        public double[] Numbers;

        private string _value;
        public override string Value
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_value))
                {
                    foreach (var v in Numbers)
                    {
                        _value += v + "\n";
                    }
                    _value = _value.Remove(_value.Length - 1, 1);
                }
                return _value;
            }

            protected set
            {
                base.Value = value;
            }
        }

        public MlDataNumArray(string rawblock)
        {
            var list = new List<double>();
            var lines = rawblock.Split('\n').ToList();
            var mul = 1.0;
            Name = lines[0];
            lines.RemoveAt(0);
            if (lines.Count == 1) // if 2 lines, horizontal array
            {
                lines = lines[0].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            if (lines[0].Contains("*"))
            {
                var zl = lines[0].Replace("*","");
                double n;
                mul = double.TryParse(zl, NumberStyles.Any, CultureInfo.InvariantCulture, out n) ? n : 1;
                lines.RemoveAt(0);
            }
            foreach(var t in lines)
            {
                double n;
                if (double.TryParse(t, NumberStyles.Any, CultureInfo.InvariantCulture, out n))
                    list.Add(n * mul);
                else
                    list.Add(-1);
            }
            Success = true;
            Numbers = list.ToArray();
        }
    }

    public class MlDataMatrix : MlDataContainer
    {
        public double[,] Matrix;

        public override string Value
        {
            get
            {
                if (Matrix != null) return $"Matrix {Matrix.GetLength(0)}x{Matrix.GetLength(1)}\nDouble click to view";
                return "Empty matrix";
            }

            protected set
            {
                base.Value = value;
            }
        }

        public MlDataMatrix(string rawblock)
        {
            string name = "";
            if (rawblock.Contains("Columns"))
                rawblock = FixColMatrix(rawblock, ref name);
            var lines = rawblock.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            if(string.IsNullOrWhiteSpace(name))
                Name = lines[0];
            Name = name;
            var sizex = lines.Length - 1;
            var sizey = lines[1].Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Length;
            Matrix = new double[sizex, sizey];
            for (var i = 1; i < sizex+1; i++)
            {
                var sline = lines[i].Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                for (var j = 0; j < sizey; j++)
                {
                    try
                    {
                        double n;
                        if (double.TryParse(sline[j], NumberStyles.Any, CultureInfo.InvariantCulture, out n))
                        {
                            Matrix[i - 1, j] = n;
                        }
                        else
                            Matrix[i - 1, j] = -1;
                    }
                    catch (Exception)
                    {
                        
                    }
                }
            }
            Success = true;
        }

        private string FixColMatrix(string raw, ref string name)
        {
            var lines = raw.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            name = lines[0];
            var blocks = new List<List<string>>();
            var hadFirst = false;

            var tmplist = new List<string>();
            foreach (var l in lines)
            {
                if (l.Contains("Colu"))
                {
                    if (!hadFirst)
                    {
                        hadFirst = true;
                        continue;
                    }
                    blocks.Add(tmplist);
                    tmplist = new List<string>();
                }
                tmplist.Add(l);
            }
            blocks.Add(tmplist);
            tmplist = null;

            var size = blocks[0].Count;
            foreach (var l in blocks)
            {
                if (l.Count != size)
                    throw new Exception("Block sizes don't match");
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 1; j < blocks.Count; j++)
                {
                    blocks[0][i] += " " + blocks[j][i];
                }
            }

            var sb = new StringBuilder();
            foreach (var b in blocks[0])
            {
                sb.AppendLine(b);
            }
            return sb.ToString();

        }

    }
}