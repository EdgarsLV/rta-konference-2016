﻿// https://bitbucket.org/neonuni/konf2016/
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace KApp
{
    /// <summary>
    /// Interaction logic for GenericLoadWindow.xaml
    /// </summary>
    public partial class GenericLoadWindow : Window
    {
        private readonly DispatcherTimer _timer = new DispatcherTimer(); // timer
        private readonly char[] _animsyms = { '|', '/', '-', '\\' }; // animation characters
        private int _i; // iterator used as animation char index
        private readonly Task _task;

        private const int TimerMs = 300;

        public GenericLoadWindow(string title, string message, Action action)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _task = new Task(action);
            InitializeComponent();
            Title = title;
            TextBlockMessage.Text = message;
            Start();
        }

        private void Start()
        {
            _timer.Tick += OnTimerTick;
            _timer.Interval = new TimeSpan(0, 0, 0, 0, TimerMs);
            _timer.Start();
            _task.Start();
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            if (_task.IsCompleted)
            {
                Close();
            }
            if (_i >= _animsyms.Length)
                _i = 0;
            TextBlockAnimated.Text = _animsyms[_i].ToString();
            _i++;
        }

        public static void LoadMatlabDialog()
        {
            var w = new GenericLoadWindow("Loading matlab, please wait", "Hold on while matlab initializes...", () => AppCore.LoadMatlab());
            w.ShowDialog();
        }
    }
}
