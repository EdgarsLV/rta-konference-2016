﻿// https://bitbucket.org/neonuni/konf2016/
using System.Windows;

namespace KApp
{
    public partial class ResultsMatrixWindow : Window
    {
        public ResultsMatrixWindow(MlDataMatrix matrix)
        {
            InitializeComponent();
            Dg2D.ItemsSource2D = matrix.Matrix;
            Title = $"{matrix.Name} {matrix.Matrix.GetLength(0)}x{matrix.Matrix.GetLength(1)}";
        }
    }
}
