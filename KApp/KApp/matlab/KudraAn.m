%1-sl�nis, u'' -a1^2 u=F0/Dz, Dz u'(0)=beta(u(0)-C0), Dz u'(L)=-alfa(u(L)-Ca)
%Exp, parab splaini un preciz atr., ja a=a1,a0=a1/2, tad exp.spl. ir prcizi ari neh vdj. 
function  KudraAn(N)
Ca=2,C0=0.3, alfa=20, beta=0, Lx=1;Ly=1;F0=0.0
Lz=1;Dx=3*10^(-4);Dy=3*10^(-4);Dz=10^(-3);h= Lz/N;NP=N+1;
%Dz=1% Piem�ram
f0=F0/Dz;
z=linspace(0,Lz,NP)';
%Analitiskais atrisin�jums
U=zeros(NP,1);U1=zeros(NP,1);U2=zeros(NP,1);
a1=pi*sqrt((Dx/Lx^2 +Dy/Ly^2)/Dz)
%a1=5% Piem�ram
f1=f0/a1^2
a=a1,a0=a1/2
p2=(alfa *(Ca-f1)+beta*(C0-f1)*( cosh(a1*Lz)+alfa*sinh(a1*Lz)/(a1*Dz)))/( cosh(a1*Lz)*(alfa+beta) +...
sinh(a1*Lz)* (a1*Dz+alfa*beta/(a1*Dz)));
p1=(p2 -C0+f1)*beta/(a1*Dz);
for j= 1:NP 
       U(j)=p1*sinh(a1*(z(j)))+p2*cosh(a1*(z(j)))+f1; 
 end
v1=1/(Lz*a1)*(p1*(cosh(a1*Lz)-1)+p2*sinh(a1*Lz));
%Exponenci�lie splaini
A0=0.25*(sinh(a0*Lz)/(a0*Lz)-1)/(cosh(a0*Lz)-1),dz=0.5*Lz*a*coth(a*Lz/2)
dz0=0.5*Lz*a0*coth(a0*Lz/2),G=Lz/Dz;
A1=0.25-A0, a11=dz*Dz+beta*Lz/2; a21=dz*Dz+alfa*Lz/2;
a12=dz0 +beta*G*A1; a22=dz0+alfa*G*A1;
det1=a11*a22+a21*a12; gz=(alfa *a11 +beta*a21)/det1, az=alfa*a11/det1;bz=beta*a21/det1;
Bz=2*dz0/Lz;
cz1=(F0+Bz*(bz*C0 +az*Ca))/(Bz*gz +a1^2*Dz)
m=(cz1*(beta*a22-alfa*a12)+Ca*alfa*a12-C0*beta*a22)/det1
e=(-cz1*gz +Ca*az+C0*bz)
for i=1:N+1
U2(i)=cz1+0.5*m*Lz*sinh(a*(z(i)-Lz/2))/sinh(a*Lz/2)+e*G*(0.25*(sinh(a0*(z(i)-Lz/2)))^2/(sinh(a0*Lz/2))^2 -A0);
end
%z0=Dz*dz*m-dz0*e-beta*(cz1-m*Lz/2+e*G*A1-C0)
%N.Bahvalova DS
%b1=1+h*beta/Dz; b2=1+h*alfa/Dz;a11=cosh(a1*h)-b1;a12=sinh(a1*h);a21=cosh(a1*(Lz-h))-b2*cosh(a1*Lz);
%a22 =sinh(a1*(Lz-h))-b2*sinh(a1*Lz);b3=h*beta/Dz*C0;b4=h*alfa/Dz*Ca;
%det= a11*a22-a12*a21;C1= (-a22*b3 +a12*b4)/det; C2= (-a11*b4 + a21*b3)/det;
%for j= 1:N+1 
 %  U3(j)=C1*cosh(a1*(z(j)))+C2*sinh(a1*(z(j))); 
 %end
%v2=1/(Lz*a1)*(C2*(cosh(a1*Lz)-1)+C1*sinh(a1*Lz));

%Kvadr�tiskie splaini
A0=1/12;dz=1;dz0=1;A1=1/6;
a11=dz*Dz+beta*Lz/2; a21=dz*Dz+alfa*Lz/2;
a12=dz0 +beta*G*A1; a22=dz0+alfa*G*A1;
det1=a11*a22+a21*a12; gz=(alfa *a11 +beta*a21)/det1, az=alfa*a11/det1;bz=beta*a21/det1;
Bz=2*dz0/Lz;
cz=(F0+Bz*(bz*C0 +az*Ca))/(Bz*gz +a1^2*Dz);
m=(cz*(beta*a22-alfa*a12)+Ca*alfa*a12-C0*beta*a22)/det1;
e=(-cz*gz +Ca*az+C0*bz);
for i=1:N+1
   U1(i)=cz + m*(z(i)-Lz/2)+e*G*((z(i)-Lz/2)^2/Lz^2-1/12);
end
Maksimumi=[max(U),max(U1),max(U2)],vidv=[v1,cz,cz1],
kluda=[max(abs(U-U1)),max(abs(U-U2))]
figure,plot(z,U,'k>',z,U1,'ko',z,U2,'k-','LineWidth',2.5, 'MarkerSize',2.5)
legend('analytic solution','quadratic spline', 'exponential spline')
%figure,plot(z,U,'k>',z,U1,'ko',z,U2,'k-',z,U3,'k--','LineWidth',2.5, 'MarkerSize',2.5)
%legend('analytic solution','quadratic spline', 'exponential spline','FDS')

