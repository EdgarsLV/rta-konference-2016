%N-slāni, Dz u''+ r u'-a0^2+F=0, Dz u'(0)=beta(u(0)-C0),Dzu'(L)=-alfa(u(L)-Ca), N=5;10
%Salidz. ar precizo atrisinājumu
function KudraAnKonN_2_1(N,M,Ca,alfa,beta)
N, M, Ca, alfa, beta
%M=60
MN=M/N
%Ca=2
C0=0.3
%alfa=10
%beta=10

Dz=0.01*[1;20;3;40;5;1;20;3;40;5],Rz=0.*[1;2;3;4;5;1;2;3;4;5],
F=-0.01*[4;8;12;16;20;4;8;12;16;20],H=0.6*ones(N,1);c0=zeros(N,1);
Lz=6;MP=M+1;N2=N-1;A00=0.1*[1;2;3;4;5;1;2;3;4;5],A0=diag(A00);mz=zeros(N,1);ez=zeros(N,1);z=linspace(0,Lz,MP)';
Bd=zeros(N,1);Bp=zeros(N2,1);Bm=zeros(N2,1);cz=zeros(N,1);HS=zeros(N,1);U=zeros(N,MP);U1=zeros(MP,1);
U2=zeros(MP,1);
for j=1:N
    HS(j)=sum(H(1:j))-H(j)/2;
end
Ad=zeros(N,1);Ap=zeros(N2,1);Am=zeros(N2,1);C1p=zeros(N2,1);C2p=zeros(N2,1);C3p=zeros(N2,1);B0=zeros(N,N);
C00=zeros(N,N);kap1=zeros(N2,1);kap2=zeros(N2,1);kap3=zeros(N2,1); del1=zeros(N2,1);del2=zeros(N2,1);
C1m=zeros(N2,1);C2m=zeros(N2,1);C3m=zeros(N2,1);Bmd=zeros(N,1);Bmm=zeros(N2,1);Cmd=zeros(N,1);Cmm=zeros(N2,1);
K1=-0.5*Rz./Dz -sqrt((0.5*Rz./Dz).^2+diag(A0)./Dz); K2=-0.5*Rz./Dz + sqrt((0.5*Rz./Dz).^2+diag(A0)./Dz);
F1m=exp(-0.5*H.*K1)-2./(H.*K1).*sinh(0.5*H.*K1);F2m=exp(-0.5*H.*K2)-2./(H.*K2).*sinh(0.5*H.*K2);
F1p=exp(0.5*H.*K1)-2./(H.*K1).*sinh(0.5*H.*K1);F2p=exp(0.5*H.*K2)-2./(H.*K2).*sinh(0.5*H.*K2);
F1am=K1.*exp(-0.5*H.*K1);F2am=K2.*exp(-0.5*H.*K2);F1ap=K1.*exp(0.5*H.*K1);F2ap=K2.*exp(0.5*H.*K2);
kap1=Dz(1:N2).*F1ap(1:N2)./(Dz(2:N).*F1am(2:N));kap2=Dz(1:N2).*F2ap(1:N2)./(Dz(2:N).*F1am(2:N)) ;
del1=F2am(2:N)./F1am(2:N);del2=F2ap(1:N2)./F1ap(1:N2);kap3=Dz(2:N).*F2am(2:N)./(Dz(1:N2).*F1ap(1:N2));
C1p=F1p(1:N2)-kap1.*F1m(2:N);C2p=F2p(1:N2)-kap2.*F1m(2:N);C3p=-F2m(2:N)+del1.*F1m(2:N);
C1m=F1p(1:N2)./kap1-F1m(2:N);C2m=F2m(2:N)-kap3.*F1p(1:N2);C3m=F2p(1:N2)-del2.*F1p(1:N2);
Bd(2:N2)=C1m(1:N2-1)+C1p(2:N2); Bp(2:N2)=-C1m(1:N2-1); Bm(1:N2-1)=-C1p(2:N2); a11m=Dz(1)*F1am(1)-beta*F1m(1);
a12m=Dz(1)*F2am(1)-beta*F2m(1);a11p= Dz(N)*F1ap(N)+alfa*F1p(N); a12p= Dz(N)*F2ap(N)+alfa*F2p(N);
Bd(1)=-a11m-beta*C1p(1);Bd(N)=a11p+alfa*C1m(N2);Bp(1)=a11m; Bm(N2)=-a11p;
Ad(2:N2)=-C2m(1:N2-1).*C1p(2:N2)-C2p(2:N2).*C1m(1:N2-1), Ap(2:N2)=-C1m(1:N2-1).*C3p(2:N2); 
Am(1:N2-1)=C1p(2:N2).*C3m(1:N2-1);
Ad(1)=C2p(1)*a11m-C1p(1)*a12m;Ad(N)=-(a12p*C1m(N2)+a11p*C2m(N2));Ap(1)=a11m*C3p(1); Am(N2)=a11p*C3m(N2);
Aze=diag(Ad)+diag(Ap',1) +diag(Am',-1), Bze=diag(Bd)+diag(Bp',1) +diag(Bm',-1); B0(1,1)=beta*C1p(1);
B0(N,N)=-alfa*C1m(N2);
B1e=Aze\Bze;B2e=Aze\B0;Bmd(2:N)=C2m(1:N2)./C1m(1:N2);Bmd(1)=-a12m/a11m;Cmd(2:N)=1./C1m(1:N2);Cmd(1)=beta/a11m;
Bmm=-C3m./C1m;Cmm=-1./C1m;Bzm=diag(Bmd)+diag(Bmm',-1);Czm=diag(Cmd)+diag(Cmm',-1); C00(1,1)=-beta/a11m;c0(1)=C0; 
c0(N)=Ca;
B1m=Bzm*B1e + Czm; B2m=Bzm*B2e + C00;Dhe=diag(2./H.*sinh(0.5*K2.*H).*(Dz.*K2+Rz));
Dhm=diag(2./H.*sinh(0.5*K1.*H).*(Dz.*K1+Rz));A1=A0 -Dhe*B1e-Dhm*B1m ;A2= Dhe*B2e+Dhm*B2m;
cz=A1\(A2*c0 +F),ez=B1e*cz +B2e*c0;mz=Bzm*ez+ Czm*cz + C00*c0 ;
for k=1:N
    for i=(k-1)*MN+1:k*MN+1
U1(i)=cz(k)+mz(k)*(exp(K1(k)*(z(i)-HS(k)))-2*sinh(K1(k)*H(k)/2)/(H(k)*K1(k)))+ez(k)*(exp(K2(k)*(z(i)-HS(k)))-2*sinh(K2(k)*H(k)/2)/(H(k)*K2(k)));
U(k,i)=cz(k)+mz(k)*(exp(K1(k)*(z(i)-HS(k)))-2*sinh(K1(k)*H(k)/2)/(H(k)*K1(k)))+ez(k)*(exp(K2(k)*(z(i)-HS(k)))-2*sinh(K2(k)*H(k)/2)/(H(k)*K2(k)));
    end
end
figure,plot(z,U1,'ko','LineWidth',2.5, 'MarkerSize',4)

%Precizais atrisinājums
F1m=exp(-0.5*H.*K1);F2m=exp(-0.5*H.*K2);F1p=exp(0.5*H.*K1);F2p=exp(0.5*H.*K2);
kap1=Dz(1:N2).*F1ap(1:N2)./(Dz(2:N).*F1am(2:N));kap2=Dz(1:N2).*F2ap(1:N2)./(Dz(2:N).*F1am(2:N)); 
del1=F2am(2:N)./F1am(2:N);del2=F2ap(1:N2)./F1ap(1:N2);kap3=Dz(2:N).*F2am(2:N)./(Dz(1:N2).*F1ap(1:N2));
C1p=F1p(1:N2)-kap1(1:N2).*F1m(2:N);C2p=F2p(1:N2)-kap2(1:N2).*F1m(2:N);C3p=-F2m(2:N)+del1(1:N2).*F1m(2:N);
C1m=F1p(1:N2)./kap1(1:N2)-F1m(2:N);C2m=F2m(2:N)-kap3(1:N2).*F1p(1:N2);C3m=F2p(1:N2)-del2(1:N2).*F1p(1:N2);
a11m=Dz(1)*F1am(1)-beta*F1m(1);
a12m=Dz(1)*F2am(1)-beta*F2m(1);a11p= Dz(N)*F1ap(N)+alfa*F1p(N); a12p= Dz(N)*F2ap(N)+alfa*F2p(N);
AF=F./A00;G=zeros(N,1);
G(2:N2)=(AF(3:N) -AF(2:N2)).*C1m(1:N2-1)+(AF(2:N2) -AF(1:N2-1)).*C1p(2:N2);
G(1)=a11m*(AF(2)-AF(1))-C1p(1)*beta*(AF(1)-C0); G(N)=a11p*(AF(N)-AF(N2))-C1m(N2)*alfa*(AF(N)-Ca);
Ad(2:N2)=C2m(1:N2-1).*C1p(2:N2)+C2p(2:N2).*C1m(1:N2-1);Ap(2:N2)=C1m(1:N2-1).*C3p(2:N2); 
Am(1:N2-1)=-C1p(2:N2).*C3m(1:N2-1);
Ad(1)=C2p(1)*a11m-C1p(1)*a12m;Ad(N)=(a12p*C1m(N2)+a11p*C2m(N2));Ap(1)=a11m*C3p(1); Am(N2)=-a11p*C3m(N2);
Aze=diag(Ad)+diag(Ap',1) +diag(Am',-1),ez=Aze \G; mz(1)=(-ez(1)*a12m +beta*(AF(1)-C0))/a11m;
mz(2:N)=(ez(2:N).*C2m(1:N2)- ez(1:N2).*C3m(1:N2) )./C1m(1:N2);
Dz(1:N2).*(mz(1:N2).*F1ap(1:N2) +ez(1:N2).*F2ap(1:N2))- Dz(2:N).*(mz(2:N).*F1am(2:N)+ez(2:N).*F2am(2:N))
for k=1:N
    for i=(k-1)*MN+1:k*MN+1
U2(i)=mz(k)*(exp(K1(k)*(z(i)-HS(k))))+ez(k)*(exp(K2(k)*(z(i)-HS(k))))+AF(k);
U(k,i)=mz(k)*(exp(K1(k)*(z(i)-HS(k))))+ez(k)*(exp(K2(k)*(z(i)-HS(k))))+AF(k);
    end
end
U(2,13),U(3,13),U(1,7),U(2,7),U(3,19),U(4,19)
kexp=max(abs(U2-U1))
figure,plot(z,U2,'ko',z,U1,'k-*','LineWidth',2.5, 'MarkerSize',4)
legend('analytic solution','exponential spline')
title(sprintf('Concentration c=c(z),ere=%6.4f',kexp))
xlabel('z'),ylabel('c')

