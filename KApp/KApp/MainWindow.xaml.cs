﻿// https://bitbucket.org/neonuni/konf2016/
using System;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace KApp
{
    public partial class MainWindow : Window
    {
        private readonly Color _colorRed = Color.FromRgb(255, 0, 0);
        private readonly Color _colorGreen = Color.FromRgb(0, 255, 0);
        private FileSystemWatcher _fsWatcher;

        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            GenericLoadWindow.LoadMatlabDialog();
            UpdateMatlabConnectionText();

            foreach (var v in PredefinedMlFunctions.Functions)
            {
                var uc = new MlFunctionFieldUc(v);
                StackPanelPredefFunctions.Children.Add(uc);
            }

            MakeUndefinedFunctionsUc();
            MakeTextFilesUc();

            WatchFolder();
        }

        public void UpdateMatlabConnectionText()
        {
            var s = AppCore.MlReady;
            if (s)
            {
                TextBlockMlConnectionStatus.Text = "[ONLINE]";
                TextBlockMlConnectionStatus.Foreground = new SolidColorBrush(_colorGreen);
                BtnInitMatlab.Visibility = Visibility.Collapsed;
                return;
            }
            TextBlockMlConnectionStatus.Text = "[OFFLINE]";
            TextBlockMlConnectionStatus.Foreground = new SolidColorBrush(_colorRed);
            BtnInitMatlab.Visibility = Visibility.Visible;

        }

        private void BtnInitMatlab_Click(object sender, RoutedEventArgs e)
        {
            GenericLoadWindow.LoadMatlabDialog();
        }

        private void BtnRefreshMatlab_Click(object sender, RoutedEventArgs e)
        {
            UpdateMatlabConnectionText();
            OnFileSystemChanged(null, null);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            UpdateMatlabConnectionText();
        }

        private void WatchFolder()
        {
            _fsWatcher = new FileSystemWatcher
            {
                Path = AppCore.GetWorkingDir(),
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.LastAccess | NotifyFilters.Security | NotifyFilters.Size,
                Filter = "*.*"
            };
            _fsWatcher.Changed += OnFileSystemChanged;
            _fsWatcher.Renamed += OnFileSystemChanged;
            _fsWatcher.Deleted += OnFileSystemChanged;
            _fsWatcher.EnableRaisingEvents = true;
        }

        private void OnFileSystemChanged(object sender, FileSystemEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                MakeUndefinedFunctionsUc();
                MakeTextFilesUc();
            });
        }

        private void MakeUndefinedFunctionsUc()
        {
            StackPanelUndefinedFuncs.Children.Clear();
            var c = 0;
            foreach (var f in Directory.GetFiles(AppCore.GetWorkingDir(), "*.m"))
            {
                var func = MlFunctionInfo.CreateFromFile(f);
                var uc = new MlFunctionFieldUc(func);
                StackPanelUndefinedFuncs.Children.Add(uc);
                c++;
            }
            ExpanderUndefinedFuncs.Header = $"Undefined Functions ({c})";
        }

        private void MakeTextFilesUc()
        {
            StackPanelTextFiles.Children.Clear();
            var c = 0;
            foreach (var f in Directory.GetFiles(AppCore.GetWorkingDir(), "*.txt"))
            {

                var uc = new MlFunctionFieldUc(f);
                StackPanelTextFiles.Children.Add(uc);
                c++;
            }
            ExpanderTextFiles.Header = $"Text Files ({c})";
        }

        private void BtnQuestion_Click(object sender, RoutedEventArgs e)
        {
            var w = new AboutWindow();
            w.Show();
        }
    }
}
