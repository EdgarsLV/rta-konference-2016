﻿// https://bitbucket.org/neonuni/konf2016/
using System.Collections.Generic;
using System.IO;

namespace KApp
{
    public class PredefinedMlFunctions
    {
        public static readonly List<MlFunctionInfo> Functions = new List<MlFunctionInfo>();

        static PredefinedMlFunctions()
        {
            var f1 = MlFunctionInfo.CreateFromFile(Path.Combine(AppCore.GetWorkingDir(), "KudraAn.m"));
            f1.OverloadArgs["N"] = "10";
            Functions.Add(f1);

            var f2 = MlFunctionInfo.CreateFromFile(Path.Combine(AppCore.GetWorkingDir(), "KudraAnKonN_2_1.m"));
            f2.OverloadArgs["N"] = "10";
            f2.OverloadArgs["M"] = "60";
            f2.OverloadArgs["Ca"] = "2";
            f2.OverloadArgs["alfa"] = "10";
            f2.OverloadArgs["beta"] = "10";
            Functions.Add(f2);
        }
    }
}