﻿// https://bitbucket.org/neonuni/konf2016/
using System.IO;
using System;
using System.Windows;

namespace KApp
{
    public class AppCore
    {
        /// <summary>
        /// Reference to matlab instance if any
        /// Do not use in most cases, prefer AppCore.Exec(...)
        /// </summary>
        public static MLApp.MLApp Ml { get; private set; }

        private static string _currentWorkingDir;

        static AppCore()
        {
            _currentWorkingDir = Path.Combine(Directory.GetCurrentDirectory(), "matlab");
        }

        /// <summary>
        /// Checks if matlab instance is ready, false if not
        /// </summary>
        public static bool MlReady
        {
            get
            {
                if (Ml == null) // if null, it can't be ready
                    return false;
                try // Ml is not null but could be a bad instance so we attempt to execute a command
                {
                    Ml.Execute("cd"); // does nothing in this context
                    return true;
                }
                catch (Exception) // if command execution failed, can assume the instance is faulty
                {
                    Ml = null; // dereference faulty instance
                    return false;
                }
            }
        }

        /// <summary>
        /// Executes a command in matlab 
        /// </summary>
        /// <param name="command">Command to execute</param>
        /// <returns>Matlab output result or -1 if matlab instance is not ready</returns>
        public static string Exec(string command)
        {
            return MlReady ? Ml.Execute(command) : "-1";
        }

        /// <summary>
        /// Loads a new matlab instance or hooks to an existing one
        /// </summary>
        /// <returns>True if connection is successful, false otherwise</returns>
        public static bool LoadMatlab()
        {
            if (Ml != null) Ml = null;
            var t = Type.GetTypeFromProgID("Matlab.Desktop.Application");
            Ml = (MLApp.MLApp)Activator.CreateInstance(t);

            if (Ml == null)
            {
                MessageBox.Show("Failed to load matlab due to reasons unknown", "Matlab load error");
                return false;
            }

            try
            {
                Ml.Execute("clc");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Matlab load exception");
                return false;
            }
            SetWorkingDir();
            return true;
        }

        /// <summary>
        /// Sets matlab working dir to default path which is {app root}/matlab
        /// </summary>
        public static void SetWorkingDir()
        {
            var p = Path.Combine(Directory.GetCurrentDirectory(), "matlab");
            SetWorkingDir(p);
        }
        /// <summary>
        /// Sets matlab working directory
        /// </summary>
        /// <param name="path">Path to said directory, must exist</param>
        public static void SetWorkingDir(string path)
        {
            if (!Directory.Exists(path))
                return;
            Exec($"cd ('{path}');");
            _currentWorkingDir = path;
        }
        /// <summary>
        /// Gets current working directory path
        /// </summary>
        /// <returns>Path to current working directory</returns>
        public static string GetWorkingDir()
        {
            if(string.IsNullOrWhiteSpace(_currentWorkingDir))
                _currentWorkingDir = Path.Combine(Directory.GetCurrentDirectory(), "matlab");
            return _currentWorkingDir;
        }

        /// <summary>
        /// Runs clear and clc in current instance to clear it
        /// </summary>
        public static void ClearMl()
        {
            Exec("clear");
            Exec("clc");
        }

        
    }
}