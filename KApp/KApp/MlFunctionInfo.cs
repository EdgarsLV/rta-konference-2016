﻿// https://bitbucket.org/neonuni/konf2016/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace KApp
{
    public class MlFunctionInfo
    {
        public string Name;
        public int OverloadArgCount => OverloadArgs.Count;
        public Dictionary<string, string> OverloadArgs = new Dictionary<string, string>();
        public int ReturnArgCount => ReturnArgs.Count;
        public List<string> ReturnArgs = new List<string>();
        public bool CreatedSuccessfully;

        public string MakeCallableString(params string[] args)
        {
            var sb = new StringBuilder();
            sb.Append(Name);
            sb.Append("(");
            for (var i = 0; i < args.Length; i++)
            {
                sb.Append(args[i]);
                if (i != args.Length - 1)
                    sb.Append(",");
            }
            sb.Append(")");
            return sb.ToString();
        }

        public static MlFunctionInfo CreateFromFile(string filepath)
        {
            if (!File.Exists(filepath))
                return new MlFunctionInfo() {CreatedSuccessfully = false};

            string l;

            var f = new StreamReader(filepath);
            while ((l = f.ReadLine()) != null)
            {
                if (l.Trim().StartsWith("function"))
                    break;
            }
            f.Close();
            return CreateFromLine(l);
        }
        public static MlFunctionInfo CreateFromString(string data)
        {
            var lines = data.Split('\n');
            foreach (var l in lines)
            {
                if (l.Trim().StartsWith("function"))
                    return CreateFromLine(l.Trim());
            }
            return new MlFunctionInfo() {CreatedSuccessfully = false};
        }
        public static MlFunctionInfo CreateFromLine(string line)
        {
            var ml = new MlFunctionInfo();
            var cline = line.Replace("function", "").Replace("=","").Trim(); // remove function and = as we don't need them
            var regRetArgs = @"\[([^]]*)\]"; // matches return args inside []
            var regOvrArgs = @"\(([^)]*)\)"; // matches overload args inside ()
            cline = Regex.Replace(cline, regRetArgs, ""); // remove return args from string
            cline = Regex.Replace(cline, regOvrArgs, ""); // remove overload args from string
            ml.Name = cline; // only name should be left now

            var rargs = Regex.Match(line, regRetArgs);
            var iargs = Regex.Match(line, regOvrArgs);

            foreach (var v in rargs.Groups[1].Value.Split(new [] {','}, StringSplitOptions.RemoveEmptyEntries))
            {
                ml.ReturnArgs.Add(v);
            }
            foreach (var v in iargs.Groups[1].Value.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
            {
                ml.OverloadArgs.Add(v, "");
            }
            ml.CreatedSuccessfully = true;
            return ml;
        }

        public static List<MlFunctionInfo> GetFromDir(string dirpath)
        {
            var l = new List<MlFunctionInfo>();
            if (!Directory.Exists(dirpath))
                return l;
            var f = Directory.GetFiles(dirpath, "*.m");
            l.AddRange(f.Select(CreateFromFile).Where(ml => ml.CreatedSuccessfully));
            return l;
        }
    }
}