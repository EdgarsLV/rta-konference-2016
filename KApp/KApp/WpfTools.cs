﻿// https://bitbucket.org/neonuni/konf2016/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace KApp
{
    public static class WpfExtensionMethods
    {
        /// <summary>
        /// Code from http://www.wpftutorial.net/INotifyPropertyChanged.html
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="memberExpression"></param>
        /// <returns></returns>
        public static bool ChangeAndNotify<T>(this PropertyChangedEventHandler handler, ref T field, T value, Expression<Func<T>> memberExpression)
        {
            if (memberExpression == null)
            {
                throw new ArgumentNullException(nameof(memberExpression));
            }
            var body = memberExpression.Body as MemberExpression;
            if (body == null)
            {
                throw new ArgumentException("Lambda must return a property.");
            }
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            var vmExpression = body.Expression as ConstantExpression;
            if (vmExpression != null)
            {
                var lambda = Expression.Lambda(vmExpression);
                var vmFunc = lambda.Compile();
                var sender = vmFunc.DynamicInvoke();

                if (handler != null)
                {
                    handler(sender, new PropertyChangedEventArgs(body.Member.Name));
                }
            }

            field = value;
            return true;
        }
    }
}